# Player Manager - Web App

This repository is the for an back-end and front-end sources for **Player Manager** application. This includes [API backend](/backend) built using `CodeIgniter 3` and front-end [web app](/frontend) built using `React JS`

Individual project `README.MD` files are inside specific folders with more details on implementation and other useful information. This `README` will guide you to setup this project locally.

## Getting Started

- To clone this repo: `git clone git@github.com:dhavalwd/player_manager.git`
- I have used `XAMPP` to setup this project. You can move this repo to `htdocs` once you clone it.
- This repo contains `player_manager.sql` file which you can import using `phpmyadmin`.
- Once you import the database, you can open terminal and run `npm run dev` command from `/frontend` folder to start the web app. This should start `webpack-dev-server` on `9001` port. URL: http://localhost:9001

### For Testing

Note: I have used `postman` to test `API` endpoints. Here is the link for [Postman Collection](https://www.getpostman.com/collections/9904727377b2f6f79b5b)

Note: I have attached dummy `json` file for you to upload. [Json file](/players.json)

